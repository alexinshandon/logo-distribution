#!/bin/bash
#
# Hack for reporting distribution of inline image types 
#
if [ -z "$1" ]
then
	echo "ERROR: must supply a SAML metadata file"
	exit 1
fi
perl -ne 'if (/(data:image.*?),/) { print "$1\n"; } ' $1 | sort | uniq -c | sort -n
