#!/usr/bin/perl -w
#
# Given a SAML metadata file, extract sizes of logos
#
# Author: alex.stuart@jisc.ac.uk
#

use XML::LibXML;
use Getopt::Long;
my $help;
my $regAuth;
my $above;
my $binning;
my $numbers;
GetOptions(
    "numbers" => \$numbers,
    "binning" => \$binning,
    "above=i" => \$above,
    "regAuth=s" => \$regAuth,
    "help" => \$help
    ) || die "GetOptions fail";

sub usage {
        my $message = shift(@_);
        if ($message) { print $message. "\n"; }
        print <<EOF;

    usage: $0 [-h] [ -r <regAuth> ] [-a <size>] [-b] [-n] <SAML metadata file>
    
    Given a SAML metadata file, extract sizes of logos

    -h                  - show this help text and exit

    -r <regAuth>        - only extract entityIDs from this registrationAuthority
    -a <size>		- prints size, entityID and registrationAuthority of entities
			  with logos above <size> bytes.
    -b			- prints statstics on binned sizes
    -n			- prints logo sizes in ascending order

    NOTE: -a, -b and -n are incompatible

EOF
}

if ($help) {
        usage;
        exit 0;
}

if ( ($above && $binning) || ($above && $numbers) || ($binning && $numbers) ) {
	usage( "ERROR: -a, -b and -n are incompatible" );
	exit 3;
}

$METADATA=$ARGV[0];

if (!$METADATA) {
    usage ("ERROR: must provide a metadata file");
    exit 1;
}

if (! -r $METADATA) {
    usage ("ERROR: must provide a readable metadata file, not $METADATA");
    exit 2;
}

my $dom = XML::LibXML->load_xml( location => $METADATA );
my $xpc = XML::LibXML::XPathContext->new( $dom );
$xpc->registerNs( 'md', 'urn:oasis:names:tc:SAML:2.0:metadata' );
$xpc->registerNs( 'mdrpi', 'urn:oasis:names:tc:SAML:metadata:rpi' );
$xpc->registerNs( 'mdui', 'urn:oasis:names:tc:SAML:metadata:ui');

my $myXPath = '//md:EntityDescriptor[md:SPSSODescriptor/md:Extensions/mdui:UIInfo/mdui:Logo|md:IDPSSODescriptor/md:Extensions/mdui:UIInfo/mdui:Logo]';
if ( $regAuth ) { $myXPath .= '[md:Extensions/mdrpi:RegistrationInfo/@registrationAuthority="' . $regAuth . '"]'; }

my @entities = $xpc->findnodes( $myXPath );
if ( $#entities == -1 ) {
        exit 0;
}

my @bins;
my @tbins;
my $total;
my $n_logos;
my %n_hash;
if ( $above ) { print "# size-of-logo	entityID	registrationAuthority\n"; }
foreach $entity (@entities) {
	my $xpc_e = XML::LibXML::XPathContext->new;
	$xpc_e->registerNs( 'md', 'urn:oasis:names:tc:SAML:2.0:metadata' );
	my $entityID = $xpc_e->findvalue('./@entityID', $entity);
	my $regAuthfromentity;

	if ( $above ) {
		my $xpc_r = XML::LibXML::XPathContext->new;
	        $xpc_r->registerNs( 'md', 'urn:oasis:names:tc:SAML:2.0:metadata' );
		$xpc_r->registerNs( 'mdrpi', 'urn:oasis:names:tc:SAML:metadata:rpi' );
		$regAuthfromentity = $xpc_r->findvalue('md:Extensions/mdrpi:RegistrationInfo/@registrationAuthority', $entity);
	}

	my $xpc_l = XML::LibXML::XPathContext->new;
	$xpc_l->registerNs( 'md', 'urn:oasis:names:tc:SAML:2.0:metadata' );
	$xpc_l->registerNs( 'mdui', 'urn:oasis:names:tc:SAML:metadata:ui');
	my @logos = $xpc_l->findnodes('md:SPSSODescriptor/md:Extensions/mdui:UIInfo/mdui:Logo', $entity);
	push @logos, $xpc_l->findnodes('md:IDPSSODescriptor/md:Extensions/mdui:UIInfo/mdui:Logo', $entity);

	foreach (@logos) {
		my $logo = ${_}->to_literal;
		if ( $logo !~ /^data:image/ ) { next; }
		my $length = length($logo);
		++$n_logos;

		if ( $numbers ) { ++$n_hash{$length}; next; }
		if ( $binning ) {
			$total += $length;
			if ( $length < 1000 ) { ++$bins[0]; $tbins[0] += $length; next; } 
			if ( $length < 10000 ) { ++$bins[1]; $tbins[1] += $length; next; } 
			if ( $length < 100000 ) { ++$bins[2]; $tbins[2] += $length; next; } 
			++$bins[3]; 
			$tbins[3] += $length;
			next; 
		}
		if ( ! $above ) {
			print "$length\n";
			next;
		}
		if ($length >= $above) { print "$length\t" . $entityID . "\t$regAuthfromentity\n"; }
	}
}

if ( $numbers ) {
	my $running;
	foreach $lsize (sort {$a <=> $b} keys %n_hash) {
		$running += $n_hash{$lsize};
		print "$lsize\t$running\n";
	}
}

if ( $binning ) {
	print "\n";
	print "Number of logos: $n_logos\n";
	print "Total size of inline logos: $total (encoded characters)\n";
	print "Size if inline replaced by 255 character URLs: " . ($n_logos * 255) . "\n";
	printf 'Reduction in size: %u characters (%.2f', ($total - ($n_logos*255)), (100-($n_logos*255)/$total); 
	print "%)\n\n";
	printf "Logo size < 1kB:\t%4s\tsize in bin: %8s\n", $bins[0], $tbins[0];
	printf "Between 1kB and 10kB:\t%4s\tsize in bin: %8s\n", $bins[1], $tbins[1];
	printf "Between 10kB and 100kB:\t%4s\tsize in bin: %8s\n", $bins[2], $tbins[2];
	printf "100 kB and over:\t%4s\tsize in bin: %8s\n", $bins[3], $tbins[3];
	print "\n";
}
